{
	"name" : "GB.M4L.SVPAutoTune",
	"version" : 1,
	"creationdate" : 3653888732,
	"modificationdate" : 3653888875,
	"viewrect" : [ 87.0, 121.0, 300.0, 474.0 ],
	"autoorganize" : 1,
	"hideprojectwindow" : 0,
	"showdependencies" : 1,
	"autolocalize" : 0,
	"contents" : 	{
		"patchers" : 		{
			"GB.M4L.SVPAutoTune.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"toplevel" : 1
			}

		}
,
		"media" : 		{
			"BandeauIRCAMax.png" : 			{
				"kind" : "imagefile",
				"local" : 1,
				"singleton" : 				{
					"bootpath" : "~/Documents/beller_docs/RIM/MAX/M4L_greg/GB.M4L.SVPAutoTune/media",
					"projectrelativepath" : "../../media"
				}

			}

		}
,
		"externals" : 		{

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 0,
	"amxdtype" : 1633771873,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : ".",
	"sortmode" : 0,
	"viewmode" : 0
}
