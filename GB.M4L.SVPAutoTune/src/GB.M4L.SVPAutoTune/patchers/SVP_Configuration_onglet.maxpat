{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 6,
			"minor" : 1,
			"revision" : 8,
			"architecture" : "x86"
		}
,
		"rect" : [ 883.0, 273.0, 507.0, 417.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 10.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial Bold",
		"gridonopen" : 0,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 0,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"boxanimatetime" : 200,
		"imprint" : 0,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"boxes" : [ 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 45.0, 35.0, 65.0, 18.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "bpatcher",
					"name" : "SVP_Configuration.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 45.0, 91.0, 146.0, 145.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -1.0, 14.0, 147.0, 150.0 ]
				}

			}
, 			{
				"box" : 				{
					"bordercolor" : [ 0.172549, 0.172549, 0.172549, 1.0 ],
					"borderoncolor" : [ 0.172549, 0.172549, 0.172549, 1.0 ],
					"clicktabcolor" : [ 1.0, 0.898039, 0.0, 1.0 ],
					"fontface" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"hovertabcolor" : [ 0.172549, 0.172549, 0.172549, 0.403922 ],
					"hovertextcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"htabcolor" : [ 0.172549, 0.172549, 0.172549, 1.0 ],
					"htextcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"id" : "obj-1",
					"margin" : 0,
					"maxclass" : "tab",
					"multiline" : 0,
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 45.0, 64.0, 146.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -1.0, 0.0, 146.0, 14.0 ],
					"spacing_x" : 3.860002,
					"spacing_y" : 0.0,
					"tabcolor" : [ 0.047059, 0.047059, 0.047059, 1.0 ],
					"tabs" : [ "Remix", "Settings" ],
					"textcolor" : [ 0.360784, 0.360784, 0.360784, 1.0 ],
					"valign" : 0
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-2::obj-213" : [ "Conf_WindowSize", "WindowSize", 22 ],
			"obj-2::obj-226" : [ "Remix_On", "Remix", 12 ],
			"obj-2::obj-211" : [ "Conf_Oversampling", "Oversampling", 23 ],
			"obj-2::obj-234" : [ "Frequnecy_Balance", "FreqBal", 8 ],
			"obj-2::obj-248" : [ "Conf_PreserveWaveform", "PreserveWaveform", 19 ],
			"obj-2::obj-19" : [ "DelayComp", "DelayComp", 12 ],
			"obj-2::obj-2" : [ "Remix_Decay", "RmxDecay", 0 ],
			"obj-2::obj-209" : [ "Conf_FftOversamp", "FftOversamp", 24 ],
			"obj-2::obj-235" : [ "Frequency_Balance_On", "FreqBalOn", 7 ],
			"obj-2::obj-13" : [ "Remix_Attacks", "RmxAtks", 0 ],
			"obj-2::obj-217" : [ "Conf_PreserveTransients", "PreserveTransients", 20 ],
			"obj-2::obj-10" : [ "Remix_SinusNoise", "RmxSinNz", 0 ],
			"obj-2::obj-3" : [ "Remix_Bandwidth", "RmxBW", 0 ]
		}
,
		"dependency_cache" : [ 			{
				"name" : "SVP_Configuration.maxpat",
				"bootpath" : "/Users/lochard/Documents/Max/Max for Live Devices/IM-SimpleTransp Project/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
 ]
	}

}
